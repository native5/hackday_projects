// Native5 Default Application Configuration
var app = (function (app, native5) {
    app.config = { 
        path:'z7gGDxv5K1383969975',   // Change the path when developing locally - same as settings.yml
        method:'POST',
        mode:'ui'
    };

    $("#hackday-form").submit(function(evt) {
        evt.preventDefault();
        var config = $.extend({}, app.config);
        var hackday = new native5.core.Service('save', app.config);
        hackday.configureHandlers(function(data) {
            alert('Your project is submitted.');
        }, function(err) {
            alert('We are facing some issues');
        });
        var project = {
            teamName : $("form #teamName").val(),
            teamMembers : $("form #teamMembers").val(),
            twitterHandle : $("form #twitterHandle").val(),
            projectName : $("form #projectName").val(),
            projectDesc : $("form #projectDesc").val(),

        };
        hackday.invoke(project);
    });
    return app;
})(app || {}, native5);

